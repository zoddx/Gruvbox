# Duvbox
This is a theme, based on the [Vim theme](https://github.com/morhetz/gruvbox), named Gruvbox.

## Installation
Use the relevant installation guides for your modding platform.

* [BeautifulDiscord](https://github.com/DTinker/discord-resources/wiki/Installing-Modifications#beautifuldiscord)
    
* [BetterDiscord](https://imgur.com/H7VyWea)
    
* [DiscordInjections](https://github.com/DiscordInjections/DiscordInjections/wiki/Theme-Tutorial)

* [Powercord](https://github.com/CircuitCodes/Duvbox/wiki/Installing-For-Powercord)

## Screenshots
### Dark

*Friends list*

![Friends List](https://tik-tok.is-bad.com/5Azd32P.png)

*Chat*
![Chat](https://tik-tok.is-bad.com/9JPL65k.png)

### Light
*Friends list*
![Friends List](https://tik-tok.is-bad.com/oypbA8r.png)

*Chat*
![Chat](https://tik-tok.is-bad.com/2dYodWa.png)

**Sorry if you're in the screenshot, I cannot edit it at this time.*


## License
This project is licensed under the MIT License.
```
Copyright 2020 CircuitRCAY

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
